/*
 * This file was automatically generated by EvoSuite
 */


import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class ConvertEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      Convert convert0 = new Convert("dvd-ntsc", "dvd-ntsc", (GUI) null);
      // Undeclared exception!
      try {
        convert0.matrix();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test1()  throws Throwable  {
      Convert convert0 = new Convert("Status", "Status", (GUI) null);
      // Undeclared exception!
      try {
        convert0.run();
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }

  @Test
  public void test2()  throws Throwable  {
      Convert convert0 = new Convert(32, ":-( /dev/dvd: me[ia is notreognized asTrecrdable DVD: 9", ":-( /dev/dvd: me[ia is notreognized asTrecrdable DVD: 9", (GUI) null);
      assertNotNull(convert0);
  }

  @Test
  public void test3()  throws Throwable  {
      Convert convert0 = new Convert(1528, "dvd-ntsc", "dvd-ntsc", (GUI) null);
      assertNotNull(convert0);
  }
}
